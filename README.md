# mHealth Tympanometry ML Classifier
The ML tympanometry tracing classifier from
Jin et al. "A Hybrid Deep Learning Approach to Identify Preventable Childhood Hearing Loss".
(under review)
![Example Tympanograms](ABC.png)

## Getting Started (BLE Client)
* Install [Miniconda](https://docs.anaconda.com/free/miniconda/miniconda-install/) on your computer.  There are specific versions for Linux, MacOS and Windows.
* Make sure you clone this repository.
* Navigate to within this repository using your local terminal shell (e.g., on a Mac, use `Terminal`), and run the following command:
```bash
conda env create -f environment.yml -p .conda
```
* If you receive an error message about `conda` not being available, you may need to restart your computer to have this executable discoverable in your `PATH`.
* Once the environment is created, launch VS Code, open this repository as a folder, and open the `ble_client.ipyng` Jupyter notebook.
* Select the `.conda` environment from the Jupyter notebook's `Select kernel` list in the upper right corner.

![select kernel](select_kernel.png)

* You can now run the notebook.
    + If you choose `Run All`, make sure that you have the `BLE` server (the handheld device or the DK) connected and powered on.
    + If you want to run the notebook on data you previously collected, you can skip the `BLE` server connection and run the notebook as is, assuming the data are in the file called `ble_out.txt`.  An example of this file--as generated using the BLE test application with the DK--is included in this repository.
    + Run the rest of the cells to load and extract the data from the file, decode the data, run the ML classifier, and then plot the output.
* You can use this notebook to test real measurement data too; just run that first cell to pull data that has been collected from the handheld device in lieu of the mobile app.

## Getting Started (Classifier Model)
The model computes the ECV, TPP, and associated uncertainties. It then classifies tympanometry tracings as type A, B, or C.
This repo contains:
* `example.ipynb`: Jupyter notebook with usage examples
* `utils.py`: code for helper functions
* `model.pt`: trained model

## Citation
```bibtex
@article{jin2022hybrid,
  title={A Hybrid Deep Learning Approach to Identify Preventable Childhood Hearing Loss},
  author={Jin, Felix Q and Huang, Ouwen and Robler, Samantha Kleindienst and Morton, Sarah and Platt, Alyssa and Egger, Joseph and Emmett, Susan D and Palmeri, Mark L}
}
```

## Funding
The study was funded by the Patient-Centered Outcomes Research Institute (PCORI AD-1602-34571) and by the Duke Global Health Institute AI Pilot Research Grant.

## License
Copyright 2022 The Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
